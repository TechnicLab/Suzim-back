using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Suzim.IntegrationTests.Infrastructure;
using Suzim.Web.Client;

namespace Suzim.IntegrationTests.Controllers;

public sealed class PaymentMethodControllerTest : IntegrationTestBase
{
    public PaymentMethodControllerTest(TestApplication app) : base(app) { }

    [Fact(DisplayName = "Создание метода оплаты. Успешное создание")]
    public async Task CreatePaymentMethod_ShouldCreate()
    {
        //Arrange 
        var guid = Guid.NewGuid();
        var request = new PaymentMethodCreateRequest() { Name = "Mir Pay" };
        await SuzimClient.PaymentMethodClient.CreatePaymentMethodAsync(guid, request);
        //Act
        var response = await SuzimContext.PaymentMethods.FirstOrDefaultAsync(x => x.Id == guid);
        //Assert
        response.Should().BeEquivalentTo(new { Id = guid, request.Name });
    }
    
    [Fact(DisplayName = "Обновление заказчика.Удачно")]
    public async Task UpdatePaymentMethod_ShouldUpdate()
    {
        //arrange
        var id = Guid.NewGuid();
        await NewPaymentMethod(id, "MirPay");

        var request = new PaymentMethodUpdateRequest()
        {
            Name = "А"
        };
        
        //actual
        await SuzimClient.PaymentMethodClient.UpdatePaymentMethodAsync(id, request);
    
        //assert
        var paymentMethod = await SuzimClient.PaymentMethodClient.GetPaymentMethodByIdAsync(id, CancellationToken.None);

        paymentMethod.Should().BeEquivalentTo(request);
    }
    
    [Fact(DisplayName = "Получение метода оплаты по ИД.Успешное получение")]
    public async Task GetPaymentMethodById_ShouldReturn()
    {
        //Arrange
        var id = Guid.NewGuid();
        var entity = await NewPaymentMethod(id, "MirPay");
        //Act
        var result = await SuzimClient.PaymentMethodClient.GetPaymentMethodByIdAsync(id);
        //Assert
        result.Should().BeEquivalentTo(new {entity.Id, entity.Name, entity.DeletedAt});
    }
    
    [Fact(DisplayName = "Программное удаление метода оплаты.Удачно")]
    public async Task DeletePaymentMethod_ShouldDelete()
    {
        //arrange
        var id = Guid.NewGuid();
        await NewPaymentMethod(id, "Mir pay");

        //actual
        await SuzimClient.PaymentMethodClient.DeletePaymentMethodAsync(id, CancellationToken.None);

        //assert
        var action = () => SuzimClient.PaymentMethodClient.GetPaymentMethodByIdAsync(id, CancellationToken.None);
        
        await action.Should().ThrowAsync<ApiException>().Where(exception
            => exception.StatusCode == StatusCodes.Status404NotFound);
    }
}