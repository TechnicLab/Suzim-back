using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Suzim.Common.Exceptions;
using Suzim.IntegrationTests.Infrastructure;
using Suzim.Store.Repositories.Abstract;
using Suzim.Web.Client;

namespace Suzim.IntegrationTests.Controllers;

public sealed class OrderControllerTest : IntegrationTestBase
{
    private readonly IOrderRepository _orderRepository;
    
    public OrderControllerTest(TestApplication app) : base(app)
    {
        _orderRepository = app.Services.GetService<IOrderRepository>() ?? throw new Exception();
    }

    [Fact(DisplayName = "Создание заказа. Успешное создание")]
    public async Task CreateOrder_ShouldCreate()
    {
        //Arrange 
        var guid = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        
        var request = new OrderCreateRequest
        {
            CustomerId = customer.Id,
            PaymentMethodId = paymentMethod.Id,
            Number = "#1",
            Address = "Ул.Пушкина"
        };
        await SuzimClient.OrderClient.CreateOrderAsync(guid, request);
        //Act
        var response = await SuzimContext.Orders.FirstOrDefaultAsync(x => x.Id == guid);
        //Assert
        response.Should().BeEquivalentTo(request);
        response?.DeletedAt.Should().BeNull();
    }
    
    [Fact(DisplayName = "Получение заказа по ИД.Успешное получение")]
    public async Task GetOrderById_ShouldReturn()
    {
        //Arrange
        var id = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        var entity = await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        //Act
        var result = await SuzimClient.OrderClient.GetOrderByIdAsync(id);
        //Assert
        result.Should().BeEquivalentTo(new { entity.Id, entity.Number, entity.Address, entity.Status });
    }
    
    [Fact(DisplayName = "Программное удаление заказа.Удачно")]
    public async Task DeleteOrder_ShouldDelete()
    {
        //arrange
        var id = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        
        //actual
        await SuzimClient.OrderClient.DeleteOrderAsync(id, CancellationToken.None);
    
        //assert
        var action = () => SuzimClient.OrderClient.GetOrderByIdAsync(id, CancellationToken.None);
        
        await action.Should().ThrowAsync<ApiException>().Where(exception
            => exception.StatusCode == StatusCodes.Status404NotFound);
    }
    
    [Fact(DisplayName = "Обновление заказа.Удачно")]
    public async Task UpdateOrder_ShouldDelete()
    {
        //arrange
        var id = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        var paymentMethod2 = await NewPaymentMethod(Guid.NewGuid(), "Mastercard");
        await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);

        var request = new OrderUpdateRequest()
        {
            Number = "#1",
            Status = OrderStatus.Canceled,
            PaymentMethodId = paymentMethod2.Id,
            Address = "г.Темиртау"
        };
        
        //actual
        await SuzimClient.OrderClient.UpdateOrderAsync(id, request);
    
        //assert
        var order = await SuzimClient.OrderClient.GetOrderByIdAsync(id, CancellationToken.None);

        order.Should().BeEquivalentTo(new{request.Status,request.Number, request.Address});
    }
    
    [Fact(DisplayName = "Добавление товара к заказу.Удачно")]
    public async Task AddProductToOrder_ShouldAdd()
    {
        //arrange
        var id = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        var product = await NewProduct(Guid.NewGuid(), "Товар", 10M, description: null);
        var order = await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        
        //actual
        await SuzimClient.OrderClient.AddProductAsync(id, product.Id);

        //assert
        var exceptedOrder = await _orderRepository.GetById(id);
        exceptedOrder.Should().BeEquivalentTo(order);
        exceptedOrder?.Products.Should().HaveCount(1);
    }
    
    [Fact(DisplayName = "Удаление товара из заказа.Удачно")]
    public async Task RemoveProductToOrder_ShouldRemove()
    {
        //arrange
        var id = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        var product = await NewProduct(Guid.NewGuid(), "Товар", 10M, description: null);
        var order = await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        order.Products.Add(product);
        _orderRepository.Update(order);
        //actual
        await SuzimClient.OrderClient.RemoveProductAsync(id, product.Id);

        //assert
        var exceptedOrder = await _orderRepository.GetById(id);
        exceptedOrder.Should().BeEquivalentTo(order);
        exceptedOrder?.Products.Should().BeEmpty();
    }
    
    [Fact(DisplayName = "Удаление товара из заказа. Не существующий заказ. Должен выкинуть исключение")]
    public async Task RemoveProductToOrder_InvalidOrder_ShouldThrowException()
    {
        //arrange
        var id = Guid.NewGuid();
        var otherId = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var product = await NewProduct(Guid.NewGuid(), "Товар", 10M, description: null);
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        
        //actual
        var act = () => SuzimClient.OrderClient.RemoveProductAsync(otherId, product.Id);
        
        //assert
        await act.Should().ThrowAsync<RecourceNotFoundException>();
    }
    
    [Fact(DisplayName = "Удаление товара из заказа. Пытаемся удалить несуществующий товар. Должен выкинуть исключение")]
    public async Task RemoveProductToOrder_InvalidProduct_ShouldThrowException()
    {
        //arrange
        var id = Guid.NewGuid();
        var otherId = Guid.NewGuid();
        var customer = await NewCustomer(Guid.NewGuid(), "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");
        var paymentMethod = await NewPaymentMethod(Guid.NewGuid(), "MirPay");
        await NewOrder(id, "№1", "ул.Пушкина", customer.Id, paymentMethod.Id);
        
        //actual
        var act = () => SuzimClient.OrderClient.RemoveProductAsync(id, otherId);
        
        //assert
        await act.Should().ThrowAsync<RecourceNotFoundException>();
    }
}