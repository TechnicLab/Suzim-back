using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Suzim.IntegrationTests.Infrastructure;
using Suzim.Web.Client;

namespace Suzim.IntegrationTests.Controllers;

public sealed class CustomerControllerTest : IntegrationTestBase
{
    public CustomerControllerTest(TestApplication app) : base(app) { }

    [Theory(DisplayName = "Создание заказчика. Успешное создание")]
    [InlineData("Зубенко", "Михаил", "Петрович", "test@mail.ru", "+7 777 777 77 77")]
    [InlineData("Зубенко", "Михаил", null, "test@mail.ru", "+7 777 777 77 77")]
    [InlineData("Зубенко", "Михаил", "Петрович", null, "+7 777 777 77 77")]
    public async Task CreateCustomer_ShouldCreate(string surname,
        string name,
        string patronymic,
        string email, 
        string phoneNumber)
    {
        //Arrange 
        var guid = Guid.NewGuid();
        var request = new CustomerCreateRequest
        {
            Surname = surname,
            Name = name,
            Patronymic = patronymic,
            Email = email,
            PhoneNumber = phoneNumber
        };
        await SuzimClient.CustomerClient.CreateCustomerAsync(guid, request);
        //Act
        var response = await SuzimContext.Customers.FirstOrDefaultAsync(x => x.Id == guid);
        //Assert
        response.Should().BeEquivalentTo(request);
        response?.DeletedAt.Should().BeNull();
    }
    
    [Fact(DisplayName = "Обновление заказчика.Удачно")]
    public async Task UpdateCustomer_ShouldUpdate()
    {
        //arrange
        var id = Guid.NewGuid();
        await NewCustomer(id, "Зубенко", "Михаил", patronymic:null, email: null,"+ 7 777 77 77");

        var request = new CustomerUpdateRequest()
        {
            PhoneNumber = "888",
            Surname = "А",
            Name = "А",
            Patronymic = "А",
            Email = "A"
        };
        
        //actual
        await SuzimClient.CustomerClient.UpdateCustomerAsync(id, request);
    
        //assert
        var customer = await SuzimClient.CustomerClient.GetCustomerByIdAsync(id, CancellationToken.None);

        customer.Should().BeEquivalentTo(request);
    }
    
    [Fact(DisplayName = "Получение заказчика по ИД.Успешное получение")]
    public async Task GetCustomerById_ShouldReturn()
    {
        //Arrange
        var id = Guid.NewGuid();
        var entity = await NewCustomer(id, "Зубенко", "Михаил", "Петрович","test@mail.ru", "+7 777 777 77 77");
        //Act
        var result = await SuzimClient.CustomerClient.GetCustomerByIdAsync(id);
        //Assert
        result.Should().BeEquivalentTo(new
            { entity.Id, entity.Surname, entity.Name, entity.Patronymic, entity.Email, entity.PhoneNumber });
    }
    
    [Fact(DisplayName = "Программное удаление заказчика.Удачно")]
    public async Task DeleteCustomer_ShouldDelete()
    {
        //arrange
        var id = Guid.NewGuid();
        await NewCustomer(id, "Зубенко", "Михаил", "Петрович","test@mail.ru", "+7 777 777 77 77");

        //actual
        await SuzimClient.CustomerClient.DeleteCustomerAsync(id, CancellationToken.None);

        //assert
        var action = () => SuzimClient.CustomerClient.GetCustomerByIdAsync(id, CancellationToken.None);
        
        await action.Should().ThrowAsync<ApiException>().Where(exception
            => exception.StatusCode == StatusCodes.Status404NotFound);
    }
}