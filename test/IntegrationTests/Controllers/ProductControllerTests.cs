using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Suzim.IntegrationTests.Infrastructure;
using Suzim.Web.Client;

namespace Suzim.IntegrationTests.Controllers;

public sealed class ProductControllerTest : IntegrationTestBase
{
    public ProductControllerTest(TestApplication app) : base(app) { }

    [Theory(DisplayName = "Создание товара. Успешное создание")]
    [InlineData("Товар", "Описание", 10)]
    [InlineData("Товар", null, 10)]
    public async Task CreateProduct_ShouldCreate(string name, string description, decimal price)
    {
        //Arrange 
        var guid = Guid.NewGuid();
        var request = new ProductCreateRequest { Name = name, Description = description, Price = price };
        await SuzimClient.ProductClient.CreateProductAsync(guid, request);
        //Act
        var response = await SuzimContext.Products.FirstOrDefaultAsync(x => x.Id == guid);
        //Assert
        response.Should().BeEquivalentTo(request);
        response?.DeletedAt.Should().BeNull();
    }
    
    [Fact(DisplayName = "Обновление товара.Удачно")]
    public async Task UpdatePaymentMethod_ShouldUpdate()
    {
        //arrange
        var id = Guid.NewGuid();
        await NewProduct(id, "Товар", 10M, "Описание" );

        var request = new ProductUpdateRequest()
        {
            Name = "А",
            Description = "A",
            Price = 1000M
        };
        
        //actual
        await SuzimClient.ProductClient.UpdateProductAsync(id, request);
    
        //assert
        var paymentMethod = await SuzimClient.ProductClient.GetProductByIdAsync(id, CancellationToken.None);

        paymentMethod.Should().BeEquivalentTo(request);
    }
    
    [Fact(DisplayName = "Получение товара по ИД.Успешное получение")]
    public async Task GetProductById_ShouldReturn()
    {
        //Arrange
        var id = Guid.NewGuid();
        var entity = await NewProduct(id, "Товар", 10M, "Описание" );
        //Act
        var result = await SuzimClient.ProductClient.GetProductByIdAsync(id);
        //Assert
        result.Should().BeEquivalentTo(new { entity.Id, entity.Name, entity.Price, entity.Description });
    }
    
    [Fact(DisplayName = "Программное удаление товара.Удачно")]
    public async Task DeleteProduct_ShouldDelete()
    {
        //Arrange
        var id = Guid.NewGuid();
        await NewProduct(id, "Товар", 10M, null);
        //Act
        await SuzimClient.ProductClient.DeleteProductAsync(id, CancellationToken.None);
        //Assert
        var action = () => SuzimClient.ProductClient.GetProductByIdAsync(id, CancellationToken.None);
        
        await action.Should().ThrowAsync<ApiException>().Where(exception
            => exception.StatusCode == StatusCodes.Status404NotFound);
    }
}