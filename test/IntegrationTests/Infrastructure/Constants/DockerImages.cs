﻿using System.Runtime.InteropServices;

namespace Suzim.IntegrationTests.Infrastructure.Constants;

internal static class DockerImages
{
    public static string PostgresSql => "postgres:latest";
}
