﻿namespace Suzim.IntegrationTests.Infrastructure.Constants;

internal static class Configurations
{
    public static string HostUri = $"Suzim:{nameof(HostUri)}";
}
