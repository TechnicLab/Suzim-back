﻿namespace Suzim.IntegrationTests.Infrastructure;

[CollectionDefinition(Name, DisableParallelization = true)]
public sealed class IntegrationTestCollection : ICollectionFixture<TestApplication>
{
    public const string Name = "Integration tests collection";

    public const string Gategory = "ContainerTests";
}
