﻿using System.Security.Principal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Suzim.IntegrationTests.Infrastructure.Constants;
using Suzim.Store.Common;
using Suzim.Store.Postgres;
using Suzim.Web.Client.DI;
using Suzim.Web.Host;
using Testcontainers.PostgreSql;

namespace Suzim.IntegrationTests.Infrastructure;

public sealed class TestApplication : WebApplicationFactory<IApiMarker>, IAsyncLifetime
{
    private const string UserName = "postgres";
    private const string Password = "1234";
    private const string ContainerNamePrefix = "Suzim_ContainerTests";
    
    private readonly PostgreSqlContainer _dbContainer =
        new PostgreSqlBuilder()
            .WithImage(DockerImages.PostgresSql)
            .WithUsername(UserName)
            .WithPassword(Password)
            .WithDatabase("Suzim")
            .WithName($"{ContainerNamePrefix}_PostgreSql_{Guid.NewGuid()}")
            .Build();

    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            var connectionString = _dbContainer.GetConnectionString();
            services.RemoveAll<DbContextOptions<SuzimContextPostgres>>();
            services.RemoveAll<DbContextOptions<IdentityContextPostgres>>();
            services.RemoveAll<ISuzimContext>();
            services.RemoveAll<SuzimContextPostgres>();
            services.AddDbContext<ISuzimContext, SuzimContextPostgres>(o => o.UseNpgsql(connectionString), ServiceLifetime.Singleton);
            services.AddDbContext<IdentityContextPostgres>(o => o.UseNpgsql(connectionString), ServiceLifetime.Singleton);
            services.AddSuzimApiClients(CreateClient);
        });
        
        return base.CreateHost(builder);
    }

    public async Task InitializeAsync()
    {
        await _dbContainer.StartAsync();
    }

    public new async Task DisposeAsync()
    {
        await _dbContainer.StopAsync();
    }
}

