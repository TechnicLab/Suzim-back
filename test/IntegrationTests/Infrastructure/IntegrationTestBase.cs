﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Suzim.Store.Common.Entities;
using Suzim.Store.Postgres;
using Suzim.Web.Client;
using OrderStatus = Suzim.Store.Common.Enums.OrderStatus;

namespace Suzim.IntegrationTests.Infrastructure;

[Collection(IntegrationTestCollection.Name)]
public class IntegrationTestBase : IAsyncLifetime
{
    private static readonly Random Random = new();
    protected readonly IFixture Fixture = new Fixture().Customize(new AutoMoqCustomization());

    protected readonly ISuzimClient SuzimClient;
    protected readonly SuzimContextPostgres SuzimContext;
    

    protected IntegrationTestBase(TestApplication app)
    {
        SuzimClient = app.Services.GetRequiredService<ISuzimClient>();
        SuzimContext = app.Services.GetRequiredService<SuzimContextPostgres>();
        SuzimContext.Database.EnsureCreated();
    }

    protected virtual async Task<PaymentMethod> NewPaymentMethod(Guid id, string paymentMethodName)
    {
        var entity = new PaymentMethod(id, "Payment method name");
        await SuzimContext.PaymentMethods.AddAsync(entity);
        await SuzimContext.SaveChangesAsync();
        
        return entity;
    }
    
    protected virtual async Task<Customer> NewCustomer(
        Guid id,
        string surname,
        string name,
        string? patronymic,
        string? email, 
        string phoneNumber)
    {
        var entity = new Customer(id, surname, name, patronymic, email, phoneNumber);
        await SuzimContext.Customers.AddAsync(entity);
        await SuzimContext.SaveChangesAsync();
        
        return entity;
    }
    
    protected virtual async Task<Product> NewProduct(Guid id, string name, decimal price, string? description)
    {
        var entity = new Product(id, name, price, description);
        await SuzimContext.Products.AddAsync(entity);
        await SuzimContext.SaveChangesAsync();
        
        return entity;
    }
    
    protected virtual async Task<Order> NewOrder(Guid id,
        string number,
        string address,
        Guid customerId,
        Guid paymentMethodId)
    {
        var entity = new Order(id, number, address, OrderStatus.Created, customerId, paymentMethodId);
        await SuzimContext.Orders.AddAsync(entity);
        await SuzimContext.SaveChangesAsync();
        
        return entity;
    }

    public virtual Task InitializeAsync() => Task.CompletedTask;

    public virtual Task DisposeAsync() => Task.CompletedTask;
    
}
