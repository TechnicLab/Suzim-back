namespace Suzim.Common.Exceptions;

public class ResourceHasDeletedException : Exception
{
    public ResourceHasDeletedException(string name, Guid id)
        : base($"{name} с ID={id} был уже удалён") { }
}