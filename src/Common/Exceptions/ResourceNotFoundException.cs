namespace Suzim.Common.Exceptions;

public class RecourceNotFoundException : Exception
{
    public RecourceNotFoundException(string name, Guid id)
        : base($"{name} с ID={id} не найден") { }
}