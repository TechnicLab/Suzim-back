using AutoMapper;
using Suzim.Business.Models.Order;
using Suzim.Web.Contracts.Models.Order;

namespace Suzim.Web.Mappers;

public sealed class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<OrderModel, OrderResponse>();
        CreateMap<OrderCreateRequest, OrderCreateModel>();
        CreateMap<OrderUpdateRequest, OrderUpdateModel>();
    }
}