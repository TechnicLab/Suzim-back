using AutoMapper;
using Suzim.Business.Models.PaymentMethod;
using Suzim.Web.Contracts.Models.PaymentMethod;

namespace Suzim.Web.Mappers;

public sealed class PaymentMethodMappingProfile : Profile
{
    public PaymentMethodMappingProfile()
    {
        CreateMap<PaymentMethodModel, PaymentMethodResponse>();
        CreateMap<PaymentMethodCreateRequest, PaymentMethodCreateModel>();
        CreateMap<PaymentMethodUpdateRequest, PaymentMethodUpdateModel>();
    }
}