using AutoMapper;
using Suzim.Business.Models.Product;
using Suzim.Web.Contracts.Models.Product;

namespace Suzim.Web.Mappers;

public sealed class ProductMappingProfile : Profile
{
    public ProductMappingProfile()
    {
        CreateMap<ProductModel, ProductResponse>();
        CreateMap<ProductCreateRequest, ProductCreateModel>();
        CreateMap<ProductUpdateRequest, ProductUpdateModel>();
    }
}