using AutoMapper;
using Suzim.Business.Models.Customer;
using Suzim.Web.Contracts.Models.Customer;

namespace Suzim.Web.Mappers;

public sealed class CustomerMappingProfile : Profile
{
    public CustomerMappingProfile()
    {
        CreateMap<CustomerModel, CustomerResponse>();
        CreateMap<CustomerCreateRequest, CustomerCreateModel>();
        CreateMap<CustomerUpdateRequest, CustomerUpdateModel>();
    }
}