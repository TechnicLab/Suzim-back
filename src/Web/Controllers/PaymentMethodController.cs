using System.ComponentModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.PaymentMethod;
using Suzim.Web.Contracts.Models;
using Suzim.Web.Contracts.Models.PaymentMethod;

namespace Suzim.Web.Controllers;

[ApiController]
[Route("api/v1/payment-methods/")]
public sealed class PaymentMethodController : ControllerBase
{
    private readonly IPaymentMethodService _paymentMethodService;
    private readonly IMapper _mapper;

    public PaymentMethodController(
        IMapper mapper,
        IPaymentMethodService paymentMethodService)
    {
        _mapper = mapper;
        _paymentMethodService = paymentMethodService;
    }

    /// <summary>
    /// Получить все методы оплаты
    /// </summary>
    /// <param name="isOnlyActive">Только активные записи</param>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpGet(Name = "GetPaymentMethods")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PageResponse<PaymentMethodResponse>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAll(
        [FromQuery] PageFilterRequest pageFilter,
        [FromQuery, DefaultValue(true)] bool isOnlyActive,
        CancellationToken token)
    {
        var result = await _paymentMethodService.GetAllAsync(pageFilter.Offset, pageFilter.Limit, isOnlyActive, token);

        return Ok(new PageResponse<PaymentMethodResponse>(_mapper.Map<IReadOnlyList<PaymentMethodResponse>>(result.Items), result.Total));
    }

    /// <summary>
    /// Получить метод оплаты по ИД
    /// </summary>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpGet("{id:guid}", Name = "GetPaymentMethodById")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentMethodResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        var result = await _paymentMethodService.GetByIdAsync(id, token);

        if (result == null)
        {
            return NotFound(id);
        }

        return Ok(_mapper.Map<PaymentMethodResponse>(result));
    }
    /// <summary>
    /// Cоздать метод оплаты
    /// </summary>
    /// <response code="201">Запись создана в БД</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpPost(Name = "CreatePaymentMethod")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create(
        [FromQuery] Guid? id,
        [FromBody] PaymentMethodCreateRequest request,
        CancellationToken token)
    {
        var createModel = _mapper.Map<PaymentMethodCreateModel>(request);
        var result = await _paymentMethodService.CreateAsync(id, createModel, token);

        return Created($"{Request.Path}/{result}",result);
    }

    /// <summary>
    /// Обновить метод оплаты
    /// </summary>
    /// <response code="200">Данные обновлены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpPut("{id:guid}", Name = "UpdatePaymentMethod")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentMethodResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update(
        [FromRoute] Guid id,
        [FromBody] PaymentMethodUpdateRequest model,
        CancellationToken token)
    {
        var updateModel = _mapper.Map<PaymentMethodUpdateModel>(model);
        var paymentMethod = await _paymentMethodService.UpdateAsync(id, updateModel, token);

        return Ok(_mapper.Map<PaymentMethodResponse>(paymentMethod));
    }

    /// <summary>
    /// Пометить метод оплаты как удаленный
    /// </summary>
    /// <response code="200">Запись удалена</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpDelete("{id:guid}", Name = "DeletePaymentMethod")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        await _paymentMethodService.Delete(id, token);
        return Ok();
    }
}
