using System.ComponentModel;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Product;
using Suzim.Web.Contracts.Models;
using Suzim.Web.Contracts.Models.Product;
using Microsoft.AspNetCore.Identity;

namespace Suzim.Web.Controllers;

[ApiController]
[Route("api/v1/products/")]
public sealed class ProductController : ControllerBase
{
    private readonly IProductService _productService;
    private readonly IMapper _mapper;
    private readonly IConfiguration _configuration;
    private readonly UserManager<IdentityUser> _userManager;
    private static readonly HttpClient _client = new HttpClient();

    public ProductController(
        IMapper mapper,
        IProductService productService,
        IConfiguration configuration,
        UserManager<IdentityUser> userManager)
    {
        _mapper = mapper;
        _productService = productService;
        _configuration = configuration;
        _userManager = userManager;
    }

    /// <summary>
    /// Получить все товары
    /// </summary>
    /// <param name="isOnlyActive">Только активные записи</param>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpGet(Name = "GetProducts")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PageResponse<ProductResponse>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAll(
        [FromQuery] PageFilterRequest pageFilter,
        [FromQuery, DefaultValue(true)] bool isOnlyActive,
        CancellationToken token)
    {
        var result = await _productService.GetAllAsync(pageFilter.Offset, pageFilter.Limit, isOnlyActive, token);

        return Ok(new PageResponse<ProductResponse>(_mapper.Map<IReadOnlyList<ProductResponse>>(result.Items), result.Total));
    }

    /// <summary>
    /// Получить товар по ИД
    /// </summary>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpGet("{id:guid}", Name = "GetProductById")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        var result = await _productService.GetByIdAsync(id, token);

        if (result == null)
        {
            return NotFound(id);
        }

        return Ok(_mapper.Map<ProductResponse>(result));
    }
    /// <summary>
    /// Cоздать товар
    /// </summary>
    /// <response code="201">Запись создана в БД</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    //[Authorize]
    [HttpPost(Name = "CreateProduct")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create(
        [FromQuery] Guid? id,
        [FromBody] ProductCreateRequest request,
        CancellationToken token)
    {
        var createModel = _mapper.Map<ProductCreateModel>(request);
        var result = await _productService.CreateAsync(id, createModel, token);

        return Created($"{Request.Path}/{result}",result);
    }

    /// <summary>
    /// Обновить товар
    /// </summary>
    /// <response code="200">Данные обновлены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpPut("{id:guid}", Name = "UpdateProduct")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update(
        [FromRoute] Guid id,
        [FromBody] ProductUpdateRequest model,
        CancellationToken token)
    {
        var updateModel = _mapper.Map<ProductUpdateModel>(model);
        var product = await _productService.UpdateAsync(id, updateModel, token);

        return Ok(_mapper.Map<ProductResponse>(product));
    }

    /// <summary>
    /// Пометить товар как удаленный
    /// </summary>
    /// <response code="200">Запись удалена</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpDelete("{id:guid}", Name = "DeleteProduct")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        await _productService.Delete(id, token);
        return Ok();
    }

    /// <summary>
    /// Добавить товар в избранное
    /// </summary>
    /// <param name="name">Наименования продукта</param>
    /// <param name="price">Цена продукта</param>
    /// <response code="201">Запись добавлена</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpPost("favourite", Name = "AddProductToFavourite")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> AddToFavourite(
        [FromQuery] string name,
        [FromQuery] decimal price)
    {
        var query = HttpUtility.ParseQueryString(string.Empty);
        query["userId"] = _userManager.GetUserId(User).ToString();
        query["name"] = name;
        query["price"] = price.ToString();
        string? parameters = query.ToString();
        var ktorUrlBase = _configuration.GetSection("KtorBaseUrl").Value;
        if (parameters != null && ktorUrlBase != null)
        {
            string url = ktorUrlBase + "/favourites?" + parameters;
            await _client.PostAsync(url, null);
            return Ok();
        }
        return BadRequest();
    }
}
