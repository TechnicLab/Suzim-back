﻿using System.ComponentModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Order;
using Suzim.Web.Contracts.Models;
using Suzim.Web.Contracts.Models.Order;

namespace Suzim.Web.Controllers;

[ApiController]
[Route("api/v1/orders/")]
public sealed class OrderController : ControllerBase
{
    private readonly IOrderService _orderService;
    private readonly IMapper _mapper;

    public OrderController(
        IMapper mapper,
        IOrderService orderService)
    {
        _mapper = mapper;
        _orderService = orderService;
    }

    /// <summary>
    /// Получить заказы
    /// </summary>
    /// <param name="isOnlyActive">Только активные записи</param>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpGet(Name = "GetOrders")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PageResponse<OrderResponse>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAll(
        [FromQuery] PageFilterRequest pageFilter,
        [FromQuery, DefaultValue(true)] bool isOnlyActive,
        CancellationToken token)
    {
        var result = await _orderService.GetAllAsync(pageFilter.Offset, pageFilter.Limit, isOnlyActive, token);

        return Ok(new PageResponse<OrderResponse>(_mapper.Map<IReadOnlyList<OrderResponse>>(result.Items), result.Total));
    }

    /// <summary>
    /// Получить заказ по ИД
    /// </summary>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpGet("{id:guid}", Name = "GetOrderById")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        var result = await _orderService.GetByIdAsync(id, token);

        if (result == null)
        {
            return NotFound(id);
        }

        return Ok(_mapper.Map<OrderResponse>(result));
    }
    /// <summary>
    /// Cоздать заказ
    /// </summary>
    /// <response code="201">Запись создана в БД</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpPost(Name = "CreateOrder")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create(
        [FromQuery] Guid? id,
        [FromBody] OrderCreateRequest request,
        CancellationToken token)
    {
        var createModel = _mapper.Map<OrderCreateModel>(request);
        var result = await _orderService.CreateAsync(id, createModel, token);

        return Created($"{Request.Path}/{result}",result);
    }

    /// <summary>
    /// Обновить заказ
    /// </summary>
    /// <response code="200">Данные обновлены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpPut("{id:guid}", Name = "UpdateOrder")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update(
        [FromRoute] Guid id,
        [FromBody] OrderUpdateRequest model,
        CancellationToken token)
    {
        var updateModel = _mapper.Map<OrderUpdateModel>(model);
        var order = await _orderService.UpdateAsync(id, updateModel, token);

        return Ok(_mapper.Map<OrderResponse>(order));
    }

    /// <summary>
    /// Пометить заказ как удаленный
    /// </summary>
    /// <response code="200">Запись удалена</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpDelete("{id:guid}", Name = "DeleteOrder")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        await _orderService.Delete(id, token);
        return Ok();
    }
    
    /// <summary>
    /// Добавить товар к заказу
    /// </summary>
    /// <response code="200">Товар добавлен к заказу</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpPut(Name = "AddProduct")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> AddProduct(
        [FromQuery] Guid orderId,
        [FromQuery]Guid productId,
        CancellationToken token)
    {
        await _orderService.AddProduct(orderId, productId, token);
        return Ok();
    }
    
    /// <summary>
    /// Удалить товар с заказа
    /// </summary>
    /// <response code="200">Товар удалён с заказа</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpDelete(Name = "RemoveProduct")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> RemoveProduct(
        [FromQuery] Guid orderId,
        [FromQuery] Guid productId,
        CancellationToken token)
    {
        await _orderService.RemoveProduct(orderId, productId, token);
        return Ok();
    }

    /// <summary>
    /// Получить эксель отчёт
    /// </summary>
    [HttpPost("excel")]
    public async Task<IActionResult> DownloadExcelReport(ExcelReportRequest request, CancellationToken token)
    {
        var bytes = await _orderService.GenerateExcelReport(
            request.StartUtc,
            request.EncUtc,
            request.IsOnlyActive,
            request.CustomerId);

        return File(bytes,
            contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            fileDownloadName: $"Orders({request.StartUtc.DateTime}-{request.EncUtc.DateTime}).xlsx");
    }
}
