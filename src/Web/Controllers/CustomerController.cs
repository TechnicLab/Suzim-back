using System.ComponentModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Customer;
using Suzim.Web.Contracts.Models;
using Suzim.Web.Contracts.Models.Customer;

namespace Suzim.Web.Controllers;

[ApiController]
[Route("api/v1/customers/")]
public sealed class CustomerController : ControllerBase
{
    private readonly ICustomerService _customerService;
    private readonly IMapper _mapper;

    public CustomerController(
        IMapper mapper,
        ICustomerService customerService)
    {
        _mapper = mapper;
        _customerService = customerService;
    }

    /// <summary>
    /// Получить всех заказчиков
    /// </summary>
    /// <param name="isOnlyActive">Только активные записи</param>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpGet(Name = "GetCustomeres")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PageResponse<CustomerResponse>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAll(
        [FromQuery] PageFilterRequest pageFilter,
        [FromQuery, DefaultValue(true)] bool isOnlyActive,
        CancellationToken token)
    {
        var result = await _customerService.GetAllAsync(pageFilter.Offset, pageFilter.Limit, isOnlyActive, token);

        return Ok(new PageResponse<CustomerResponse>(_mapper.Map<IReadOnlyList<CustomerResponse>>(result.Items), result.Total));
    }

    /// <summary>
    /// Получить заказчика по ИД
    /// </summary>
    /// <response code="200">Данные получены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpGet("{id:guid}", Name = "GetCustomerById")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        var result = await _customerService.GetByIdAsync(id, token);

        if (result == null)
        {
            return NotFound(id);
        }

        return Ok(_mapper.Map<CustomerResponse>(result));
    }
    /// <summary>
    /// Cоздать заказчика
    /// </summary>
    /// <response code="201">Запись создана в БД</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    [HttpPost(Name = "CreateCustomer")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create(
        [FromQuery] Guid? id,
        [FromBody] CustomerCreateRequest request,
        CancellationToken token)
    {
        var createModel = _mapper.Map<CustomerCreateModel>(request);
        var result = await _customerService.CreateAsync(id, createModel, token);

        return Created($"{Request.Path}/{result}",result);
    }

    /// <summary>
    /// Обновить заказчика
    /// </summary>
    /// <response code="200">Данные обновлены</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpPut("{id:guid}", Name = "UpdateCustomer")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update(
        [FromRoute] Guid id,
        [FromBody] CustomerUpdateRequest model,
        CancellationToken token)
    {
        var updateModel = _mapper.Map<CustomerUpdateModel>(model);
        var customer = await _customerService.UpdateAsync(id, updateModel, token);

        return Ok(_mapper.Map<CustomerResponse>(customer));
    }

    /// <summary>
    /// Пометить заказчика как удаленный
    /// </summary>
    /// <response code="200">Запись удалена</response>
    /// <response code="400">Ошибка валидации входных данных</response>
    /// <response code="404">Запись не найдена в БД</response>
    [HttpDelete("{id:guid}", Name = "DeleteCustomer")]
    //[Authorize]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid id,
        CancellationToken token)
    {
        await _customerService.Delete(id, token);
        return Ok();
    }
}
