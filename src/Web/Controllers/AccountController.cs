using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Suzim.Web.Controllers;

[ApiController]
[Route("api/v1/account/")]
public sealed class AccountController: ControllerBase
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly SignInManager<IdentityUser> _signInManager;

    public AccountController(
        UserManager<IdentityUser> userManager, 
        SignInManager<IdentityUser> signInManager)
    {
        _userManager = userManager;
        _signInManager = signInManager;
    }
    
    /// <summary>
    /// Зарегистрироваться
    /// </summary>
    /// <param name="username">Имя пользователя</param>
    /// <param name="password">Пароль</param>
    [HttpPut]
    public async Task<string> Register(string username, string password)
    {
        var user = new IdentityUser() { UserName = username }; 
        var result = await _userManager.CreateAsync(user, password);
        if (result.Succeeded)
        {
            await _signInManager.SignInAsync(user, false);
            return "Пользователь успешно зарегистрирован";
        }

        var message = "";
        foreach (var error in result.Errors)
        {
            message += error.Description + ";";
        }

        return message;
    }
    
    /// <summary>
    /// Залогиниться
    /// </summary>
    /// <param name="username">Имя пользователя</param>
    /// <param name="password">Пароль</param>
    [HttpPost]
    public async Task<string> Login(string username, string password)
    {
        var result = await _signInManager.PasswordSignInAsync(username, password, false, false);
        return result.Succeeded ? "Пользователь успешно авторизован" : "Неверный логин или пароль";
    }
    
    /// <summary>
    /// Разлогиниться
    /// </summary>
    [HttpDelete]
    //[Authorize]
    public async Task<IActionResult> Logout()
    {
        await _signInManager.SignOutAsync();
        return Ok();
    }
}