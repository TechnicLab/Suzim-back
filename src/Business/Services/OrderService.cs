using AutoMapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Order;
using Suzim.Common.Exceptions;
using Suzim.Store.Common.Entities;
using Suzim.Store.Common.Enums;
using Suzim.Store.Repositories.Abstract;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Services;

internal sealed class OrderService : IOrderService
{
    private readonly IOrderRepository _orderRepository;
    private readonly IProductRepository _productRepository;
    private readonly IMapper _mapper;
    
    public OrderService(
        IOrderRepository orderRepository,
        IMapper mapper, IProductRepository productRepository)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _productRepository = productRepository;
    }

    public async Task<Guid> CreateAsync(Guid? id, OrderCreateModel model, CancellationToken token)
    {
        var order = new Order(
            id ?? Guid.NewGuid(), 
            model.Number,
            model.Address,
            OrderStatus.Created,
            model.CustomerId,
            model.PaymentMethodId);
        
        _orderRepository.Add(order);

        return await Task.FromResult(order.Id);
    }

    public async Task<OrderModel?> GetByIdAsync(Guid id, CancellationToken token)
    {
        var order = await _orderRepository.GetById(id, isOnlyActive: true, token);
        if (order == null)
        {
            return null;
        }
        var result = _mapper.Map<OrderModel>(order);

        return result;
    }

    public async Task<PageResponse<OrderModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token)
    {
        var (total, items)  = await _orderRepository.GetAll(offset, limit, isOnlyActive, token);

        if (items.Count == 0)
        {
            return new PageResponse<OrderModel>(Array.Empty<OrderModel>(), total);
        }

        var resultModels = items.Select(order => _mapper.Map<OrderModel>(order)).ToList();

        return new PageResponse<OrderModel>(resultModels, total);
    }

    public async Task<OrderModel> UpdateAsync(Guid id, OrderUpdateModel model, CancellationToken token)
    {
        var order = await GetOrderById(id, isOnlyActive: false, token);

        if (order.DeletedAt.HasValue)
        {
            throw new InvalidOperationException($"Метод оплаты с ИД {id} был удалён, редактирование не возможно.");
        }
        _mapper.Map(model, order);
        _orderRepository.Update(order);

        var result = _mapper.Map<OrderModel>(order);
        
        return result;
    }

    public async Task Delete(Guid id, CancellationToken token)
    {
        var order = await GetOrderById(id, isOnlyActive:false, token);

        if (order.DeletedAt.HasValue)
        {
            throw new ResourceHasDeletedException(nameof(Order), id);
        }

        _orderRepository.Delete(order);

        await Task.CompletedTask;
    }

    public async Task AddProduct(Guid orderId, Guid productId, CancellationToken token)
    {
        var order = await GetOrderById(orderId, true, token);
        var product = await GetProductById(productId, true, token);

        order.Products.Add(product);
        _orderRepository.Update(order);
    }

    public async Task RemoveProduct(Guid orderId, Guid productId, CancellationToken token)
    {
        var order = await GetOrderById(orderId, true, token);
        var product = await GetProductById(productId, true, token);

        if (!order.Products.Remove(product))
        {
            throw new Exception("В заказе нет этого товара");
        }
        _orderRepository.Update(order);
    }

    public async Task<byte[]> GenerateExcelReport(DateTimeOffset startUtc, DateTimeOffset endUtc, bool isOnlyActive, Guid? customerId)
    {
        var orders = (await _orderRepository.GetAll(offset: 0, limit: int.MaxValue, isOnlyActive)).items
            .Where(x => x.CreatedAt >= startUtc)
            .Where(x => x.CreatedAt <= endUtc);

        if (customerId.HasValue)
        {
            orders = orders.Where(x => x.CustomerId == customerId).ToList();
        }

        using (var package = new ExcelPackage())
        {
            var worksheet = package.Workbook.Worksheets.Add("Orders");
            worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            
            SetColumnHeaders(worksheet);

            var row = 2;
            foreach (var order in orders)
            {
                worksheet.Cells[row, 1].Value = order.Number;
                worksheet.Cells[row, 2].Value = order.Customer?.Email;
                worksheet.Cells[row, 3].Value = order.Address;
                worksheet.Cells[row, 4].Value = order.Status;
                worksheet.Cells[row, 5].Value = order.PaymentMethod?.Name;
                worksheet.Cells[row, 6].Value = ProductsInfo(order);
                worksheet.Cells[row, 6].Style.WrapText = true;
                worksheet.Cells[row, 7].Value = order.CreatedAt;
                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                worksheet.Columns[6].Width = 70.71d;
                
                row++;
            }

            return await package.GetAsByteArrayAsync();
        }
    }

    private string ProductsInfo(Order order)
    {
        var productsInfo = "";
        var number = 1;
        foreach (var product in order.Products)
        {
            productsInfo += $"{number}){product.Name}\n Price: {product.Price}\n Description: {product.Description}\n";
            number++;
        }

        return productsInfo;
    }

    private static void SetColumnHeaders(ExcelWorksheet worksheet)
    {
        worksheet.Cells[1, 1].Value = "Номер заказа";
        worksheet.Cells[1, 2].Value = "email заказчика";
        worksheet.Cells[1, 3].Value = "Адрес доставки";
        worksheet.Cells[1, 4].Value = "Статус заказа";
        worksheet.Cells[1, 5].Value = "Способ оплаты";
        worksheet.Cells[1, 6].Value = "Товары";
        worksheet.Cells[1, 7].Value = "Дата создания заказа";
    }

    private async Task<Order> GetOrderById(Guid id, bool isOnlyActive, CancellationToken token)
        => await _orderRepository.GetById(id, isOnlyActive, token)
           ?? throw new RecourceNotFoundException(nameof(Order), id);
    
    private async Task<Product> GetProductById(Guid id, bool isOnlyActive, CancellationToken token)
        => await _productRepository.GetById(id, isOnlyActive, token)
           ?? throw new RecourceNotFoundException(nameof(Product), id);
}