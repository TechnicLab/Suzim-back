using AutoMapper;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.PaymentMethod;
using Suzim.Common.Exceptions;
using Suzim.Store.Common.Entities;
using Suzim.Store.Repositories.Abstract;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Services;

internal sealed class PaymentMethodService : IPaymentMethodService
{
    private readonly IPaymentMethodRepository _paymentMethodRepository;
    private readonly IMapper _mapper;
    
    public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository, IMapper mapper)
    {
        _paymentMethodRepository = paymentMethodRepository;
        _mapper = mapper;
    }
    
    public async Task<Guid> CreateAsync(Guid? id, PaymentMethodCreateModel model, CancellationToken token)
    {
        var paymentMethod = new PaymentMethod(id ?? Guid.NewGuid(), model.Name);
        _paymentMethodRepository.Add(paymentMethod);

        return await Task.FromResult(paymentMethod.Id);
    }

    public async Task<PaymentMethodModel?> GetByIdAsync(Guid id, CancellationToken token)
    {
        var paymentMethod = await _paymentMethodRepository.GetById(id, isOnlyActive: true, token);
        if (paymentMethod == null)
        {
            return null;
        }
        var result = _mapper.Map<PaymentMethodModel>(paymentMethod);

        return result;
    }

    public async Task<PageResponse<PaymentMethodModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token)
    {
        var (total, items)  = await _paymentMethodRepository.GetAll(offset, limit, isOnlyActive, token);

        if (items.Count == 0)
        {
            return new PageResponse<PaymentMethodModel>(Array.Empty<PaymentMethodModel>(), total);
        }

        var resultModels = items.Select(box => _mapper.Map<PaymentMethodModel>(box)).ToList();

        return new PageResponse<PaymentMethodModel>(resultModels, total);
    }

    public async Task<PaymentMethodModel> UpdateAsync(Guid id, PaymentMethodUpdateModel model, CancellationToken token)
    {
        var paymentMethod = await GetPaymentMethodById(id, isOnlyActive: false, token);

        if (paymentMethod.DeletedAt.HasValue)
        {
            throw new InvalidOperationException($"Метод оплаты с ИД {id} был удалён, редактирование не возможно.");
        }
        _mapper.Map(model, paymentMethod);
        _paymentMethodRepository.Update(paymentMethod);

        var result = _mapper.Map<PaymentMethodModel>(paymentMethod);
        
        return result;
    }

    public async Task Delete(Guid id, CancellationToken token)
    {
        var box = await GetPaymentMethodById(id, isOnlyActive:false, token);

        if (box.DeletedAt.HasValue)
        {
            throw new ResourceHasDeletedException(nameof(PaymentMethod), id);
        }

        _paymentMethodRepository.Delete(box);

        await Task.CompletedTask;
    }
    
    private async Task<PaymentMethod> GetPaymentMethodById(Guid id, bool isOnlyActive, CancellationToken token)
        => await _paymentMethodRepository.GetById(id, isOnlyActive, token)
           ?? throw new RecourceNotFoundException(nameof(PaymentMethod), id);
}