using AutoMapper;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Product;
using Suzim.Common.Exceptions;
using Suzim.Store.Common.Entities;
using Suzim.Store.Repositories.Abstract;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Services;

internal sealed class ProductService : IProductService
{
    private readonly IProductRepository _productRepository;
    private readonly IMapper _mapper;
    
    public ProductService(IProductRepository productRepository, IMapper mapper)
    {
        _productRepository = productRepository;
        _mapper = mapper;
    }
    
    public async Task<Guid> CreateAsync(Guid? id, ProductCreateModel model, CancellationToken token)
    {
        var product = new Product(id ?? Guid.NewGuid(), model.Name, model.Price, model.Description);
        _productRepository.Add(product);

        return await Task.FromResult(product.Id);
    }

    public async Task<ProductModel?> GetByIdAsync(Guid id, CancellationToken token)
    {
        var product = await _productRepository.GetById(id, isOnlyActive: true, token);
        if (product == null)
        {
            return null;
        }
        var result = _mapper.Map<ProductModel>(product);

        return result;
    }

    public async Task<PageResponse<ProductModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token)
    {
        var (total, items)  = await _productRepository.GetAll(offset, limit, isOnlyActive, token);

        if (items.Count == 0)
        {
            return new PageResponse<ProductModel>(Array.Empty<ProductModel>(), total);
        }

        var resultModels = items.Select(box => _mapper.Map<ProductModel>(box)).ToList();

        return new PageResponse<ProductModel>(resultModels, total);
    }

    public async Task<ProductModel> UpdateAsync(Guid id, ProductUpdateModel model, CancellationToken token)
    {
        var product = await GetProductById(id, isOnlyActive: false, token);

        if (product.DeletedAt.HasValue)
        {
            throw new InvalidOperationException($"Метод оплаты с ИД {id} был удалён, редактирование не возможно.");
        }
        _mapper.Map(model, product);
        _productRepository.Update(product);

        var result = _mapper.Map<ProductModel>(product);
        
        return result;
    }

    public async Task Delete(Guid id, CancellationToken token)
    {
        var box = await GetProductById(id, isOnlyActive:false, token);

        if (box.DeletedAt.HasValue)
        {
            throw new ResourceHasDeletedException(nameof(Product), id);
        }

        _productRepository.Delete(box);

        await Task.CompletedTask;
    }
    
    private async Task<Product> GetProductById(Guid id, bool isOnlyActive, CancellationToken token)
        => await _productRepository.GetById(id, isOnlyActive, token)
           ?? throw new RecourceNotFoundException(nameof(Product), id);
}