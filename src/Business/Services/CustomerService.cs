using AutoMapper;
using Suzim.Business.Abstract.Services;
using Suzim.Business.Models.Customer;
using Suzim.Common.Exceptions;
using Suzim.Store.Common.Entities;
using Suzim.Store.Repositories.Abstract;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Services;

internal sealed class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    
    public CustomerService(
        ICustomerRepository customerRepository,
        IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }
    
    public async Task<Guid> CreateAsync(Guid? id, CustomerCreateModel model, CancellationToken token)
    {
        var customer = new Customer(id ?? Guid.NewGuid(),
            model.Surname,
            model.Name,
            model.Patronymic,
            model.Email,
            model.PhoneNumber);
        
        _customerRepository.Add(customer);

        return await Task.FromResult(customer.Id);
    }

    public async Task<CustomerModel?> GetByIdAsync(Guid id, CancellationToken token)
    {
        var customer = await _customerRepository.GetById(id, isOnlyActive: true, token);
        if (customer == null)
        {
            return null;
        }
        var result = _mapper.Map<CustomerModel>(customer);

        return result;
    }

    public async Task<PageResponse<CustomerModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token)
    {
        var (total, items)  = await _customerRepository.GetAll(offset, limit, isOnlyActive, token);

        if (items.Count == 0)
        {
            return new PageResponse<CustomerModel>(Array.Empty<CustomerModel>(), total);
        }

        var resultModels = items.Select(box => _mapper.Map<CustomerModel>(box)).ToList();

        return new PageResponse<CustomerModel>(resultModels, total);
    }

    public async Task<CustomerModel> UpdateAsync(Guid id, CustomerUpdateModel model, CancellationToken token)
    {
        var customer = await GetCustomerById(id, isOnlyActive: false, token);

        if (customer.DeletedAt.HasValue)
        {
            throw new InvalidOperationException($"Метод оплаты с ИД {id} был удалён, редактирование не возможно.");
        }

        _mapper.Map(model, customer);
        _customerRepository.Update(customer);

        var result = _mapper.Map<CustomerModel>(customer);
        
        return result;
    }

    public async Task Delete(Guid id, CancellationToken token)
    {
        var box = await GetCustomerById(id, isOnlyActive:false, token);

        if (box.DeletedAt.HasValue)
        {
            throw new ResourceHasDeletedException(nameof(Customer), id);
        }

        _customerRepository.Delete(box);

        await Task.CompletedTask;
    }
    
    private async Task<Customer> GetCustomerById(Guid id, bool isOnlyActive, CancellationToken token)
        => await _customerRepository.GetById(id, isOnlyActive, token)
           ?? throw new RecourceNotFoundException(nameof(Customer), id);
}