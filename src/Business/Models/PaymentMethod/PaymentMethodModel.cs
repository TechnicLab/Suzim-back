namespace Suzim.Business.Models.PaymentMethod;

public sealed class PaymentMethodModel
{
    public PaymentMethodModel(
        Guid id, 
        string name)
    {
        Id = id;
        Name = name;
    }

    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}