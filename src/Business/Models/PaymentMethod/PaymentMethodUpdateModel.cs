namespace Suzim.Business.Models.PaymentMethod;

public sealed class PaymentMethodUpdateModel
{
    public PaymentMethodUpdateModel(string name)
    {
        Name = name;
    }

    public string Name { get; set; }
}