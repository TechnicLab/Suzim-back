namespace Suzim.Business.Models.PaymentMethod;

public sealed class PaymentMethodCreateModel
{
    public PaymentMethodCreateModel(string name)
    {
        Name = name;
    }

    public string Name { get; set; }
}