namespace Suzim.Business.Models.Product;

public class ProductUpdateModel
{
    public ProductUpdateModel(string name, decimal price, string? description)
    {
        Name = name;
        Price = price;
        Description = description;
    }

    public string Name { get; set; }
    
    public decimal Price { get; set; }
    
    public string? Description { get; set; }
}