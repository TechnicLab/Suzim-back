namespace Suzim.Business.Models.Product;

public class ProductModel
{
    public ProductModel(Guid id, string name, decimal price, string? description)
    {
        Id = id;
        Name = name;
        Price = price;
        Description = description;
    }

    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public decimal Price { get; set; }
    
    public string? Description { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}