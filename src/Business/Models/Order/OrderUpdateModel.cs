using Suzim.Store.Common.Enums;

namespace Suzim.Business.Models.Order;

public sealed class OrderUpdateModel
{
    public OrderUpdateModel(string number, Guid paymentMethodId, OrderStatus status, string address)
    {
        Number = number;
        PaymentMethodId = paymentMethodId;
        Status = status;
        Address = address;
    }
    
    /// <summary>
    /// Номер заказа
    /// </summary>
    public string Number { get; set; }

    /// <summary>
    /// Способ оплаты
    /// </summary>
    public Guid PaymentMethodId { get; set; }
    

    /// <summary>
    /// Статус заказа
    /// </summary>
    public OrderStatus Status { get; set; }
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string Address { get; set; }
}