using Suzim.Business.Models.Customer;
using Suzim.Business.Models.PaymentMethod;
using Suzim.Business.Models.Product;
using Suzim.Store.Common.Enums;

namespace Suzim.Business.Models.Order;

public sealed class OrderModel
{
    public OrderModel(Guid id, string number, CustomerModel customer, PaymentMethodModel paymentMethod, OrderStatus status, string address)
    {
        Id = id;
        Number = number;
        Customer = customer;
        PaymentMethod = paymentMethod;
        Status = status;
        Address = address;
    }

    public Guid Id { get; set; }
    
    /// <summary>
    /// Номер заказа
    /// </summary>
    public string Number { get; set; }
    
    /// <summary>
    /// Заказчик
    /// </summary>
    public CustomerModel Customer { get; set; }

    /// <summary>
    /// Способ оплаты
    /// </summary>
    public PaymentMethodModel PaymentMethod { get; set; }

    public IReadOnlyList<ProductModel> Products { get; set; } = Array.Empty<ProductModel>();

    /// <summary>
    /// Статус заказа
    /// </summary>
    public OrderStatus? Status { get; set; }
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string Address { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
    
}