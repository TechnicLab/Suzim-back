using Suzim.Store.Common.Enums;

namespace Suzim.Business.Models.Order;

public sealed class OrderCreateModel
{
    public OrderCreateModel(string number, Guid customerId, Guid paymentMethodId, string address)
    {
        Number = number;
        CustomerId = customerId;
        PaymentMethodId = paymentMethodId;
        Address = address;
    }
    /// <summary>
    /// Номер заказа
    /// </summary>
    public string Number { get; set; }
    
    /// <summary>
    /// Заказчик
    /// </summary>
    public Guid CustomerId { get; set; }

    /// <summary>
    /// Способ оплаты
    /// </summary>
    public Guid PaymentMethodId { get; set; }

    /// <summary>
    /// Статус заказа
    /// </summary>
    public OrderStatus? Status { get; set; } = OrderStatus.Created;
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string Address { get; set; }
}