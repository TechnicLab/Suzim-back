namespace Suzim.Business.Models.Customer;

public class CustomerModel
{
    public CustomerModel(Guid id, string surname, string name, string? patronymic, string phoneNumber, string? email)
    {
        Id = id;
        Surname = surname;
        Name = name;
        Patronymic = patronymic;
        PhoneNumber = phoneNumber;
        Email = email;
    }

    public Guid Id { get; set; }
    
    public string Surname { get; set; }
    
    public string Name { get; set; }
    
    public string? Patronymic { get; set; }
    
    public string PhoneNumber { get; set; }
    
    public string? Email { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}