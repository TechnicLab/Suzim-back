namespace Suzim.Business.Models.Customer;

public class CustomerUpdateModel
{
    public CustomerUpdateModel(string surname, string name, string? patronymic, string phoneNumber, string? email)
    {
        Surname = surname;
        Name = name;
        Patronymic = patronymic;
        PhoneNumber = phoneNumber;
        Email = email;
    }

    public string Surname { get; set; }
    
    public string Name { get; set; }
    
    public string? Patronymic { get; set; }
    
    public string PhoneNumber { get; set; }
    
    public string? Email { get; set; }
}