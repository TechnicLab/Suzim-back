using Suzim.Business.Models.PaymentMethod;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Abstract.Services;

public interface IPaymentMethodService : IBusinessService
{
    Task<Guid> CreateAsync(Guid? id, PaymentMethodCreateModel model, CancellationToken token);
    
    Task<PaymentMethodModel?> GetByIdAsync(Guid id, CancellationToken token);
    
    Task<PageResponse<PaymentMethodModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token);
    
    Task<PaymentMethodModel> UpdateAsync(Guid id, PaymentMethodUpdateModel model, CancellationToken token);
    
    Task Delete(Guid id, CancellationToken token);
}