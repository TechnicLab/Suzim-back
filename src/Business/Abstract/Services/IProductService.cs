using Suzim.Business.Models.Product;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Abstract.Services;

public interface IProductService : IBusinessService
{
    Task<Guid> CreateAsync(Guid? id, ProductCreateModel model, CancellationToken token);
    
    Task<ProductModel?> GetByIdAsync(Guid id, CancellationToken token);
    
    Task<PageResponse<ProductModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token);
    
    Task<ProductModel> UpdateAsync(Guid id, ProductUpdateModel model, CancellationToken token);
    
    Task Delete(Guid id, CancellationToken token);
}