using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Suzim.Business.Models.Order;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Abstract.Services;

public interface IOrderService : IBusinessService
{
    Task<PageResponse<OrderModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token);
    
    Task<OrderModel?> GetByIdAsync(Guid id, CancellationToken token);

    Task<Guid> CreateAsync(Guid? id, OrderCreateModel model, CancellationToken token);

    Task<OrderModel> UpdateAsync(Guid id, OrderUpdateModel model, CancellationToken token);
  
    Task Delete(Guid id, CancellationToken token);

    Task AddProduct(Guid orderId, Guid productId, CancellationToken token);
    
    Task RemoveProduct(Guid orderId, Guid productId, CancellationToken token);

    Task<byte[]> GenerateExcelReport(DateTimeOffset startUtc, DateTimeOffset endUtc, bool isOnlyActive, Guid? customerId);

}