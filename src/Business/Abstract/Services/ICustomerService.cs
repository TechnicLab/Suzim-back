using Suzim.Business.Models.Customer;
using Suzim.Web.Contracts.Models;

namespace Suzim.Business.Abstract.Services;

public interface ICustomerService : IBusinessService
{
    Task<PageResponse<CustomerModel>> GetAllAsync(int offset, int limit, bool isOnlyActive, CancellationToken token);
    
    Task<CustomerModel?> GetByIdAsync(Guid id, CancellationToken token);

    Task<Guid> CreateAsync(Guid? id, CustomerCreateModel model, CancellationToken token);

    Task<CustomerModel> UpdateAsync(Guid id, CustomerUpdateModel model, CancellationToken token);
  
    Task Delete(Guid id, CancellationToken token);
}