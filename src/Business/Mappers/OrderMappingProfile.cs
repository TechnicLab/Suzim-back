using AutoMapper;
using Suzim.Business.Models.Order;
using Suzim.Store.Common.Entities;

namespace Suzim.Business.Mappers;

public sealed class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<Order, OrderModel>();
        CreateMap<OrderCreateModel, Order>();
        CreateMap<OrderUpdateModel, Order>();
    }
}