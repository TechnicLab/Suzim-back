using AutoMapper;
using Suzim.Business.Models.PaymentMethod;
using Suzim.Store.Common.Entities;

namespace Suzim.Business.Mappers;

public sealed class PaymentMethodMappingProfile : Profile
{
    public PaymentMethodMappingProfile()
    {
        CreateMap<PaymentMethod, PaymentMethodModel>();
        CreateMap<PaymentMethodCreateModel, PaymentMethod>();
        CreateMap<PaymentMethodUpdateModel, PaymentMethod>();
    }
}