using AutoMapper;
using Suzim.Business.Models.Product;
using Suzim.Store.Common.Entities;

namespace Suzim.Business.Mappers;

public sealed class ProductMappingProfile : Profile
{
    public ProductMappingProfile()
    {
        CreateMap<Product, ProductModel>();
        CreateMap<ProductCreateModel, Product>();
        CreateMap<ProductUpdateModel, Product>();
    }
}