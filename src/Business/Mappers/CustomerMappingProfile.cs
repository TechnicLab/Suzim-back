using AutoMapper;
using Suzim.Business.Models.Customer;
using Suzim.Store.Common.Entities;

namespace Suzim.Business.Mappers;

public sealed class CustomerMappingProfile : Profile
{
    public CustomerMappingProfile()
    {
        CreateMap<Customer, CustomerModel>();
        CreateMap<CustomerCreateModel, Customer>();
        CreateMap<CustomerUpdateModel, Customer>();
    }
}