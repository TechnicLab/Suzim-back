using Microsoft.EntityFrameworkCore;
using Suzim.Store.Common.Entities;
using Suzim.Store.Postgres;
using Suzim.Store.Repositories.Abstract;
using Suzim.Store.Repositories.Abstract.Shared;

namespace Suzim.Store.Repositories.Implementations;

internal sealed class OrderRepository : Repository<Order>, IOrderRepository
{
    public OrderRepository(SuzimContextPostgres dbContext) : base(dbContext) { }

    public override async Task<Order?> GetById(Guid id, bool isOnlyActive = true, CancellationToken token = default)
    {
        var query = DbSet.AsQueryable();
        if (isOnlyActive)
        {
            query = query.NotDeletedAt();
        }

        return await query.Include(x => x.PaymentMethod).Include(x=>x.Customer).Include(x=>x.Products).FirstOrDefaultAsync(x => x.Id == id, token);
    }

    public override async Task<(int total, IReadOnlyList<Order> items)> GetAll(int offset = 0, int limit = 100, bool isOnlyActive = true, CancellationToken token = default)
    {
        var query = DbSet.AsQueryable();

        if (isOnlyActive)
        {
            query = query.Where(x => !x.DeletedAt.HasValue);
        }

        var count = await query.CountAsync(token);

        if (count > offset)
        {
            query = query
                .Skip(offset)
                .Take(limit);
        }

        var result = await query
            .Include(x=>x.PaymentMethod)
            .Include(x=>x.Customer)
            .Include(x=>x.Products)
            .ToListAsync(token);

        return (count, result);
    }
}