using Suzim.Store.Common.Entities;
using Suzim.Store.Postgres;
using Suzim.Store.Repositories.Abstract;
using Suzim.Store.Repositories.Abstract.Shared;

namespace Suzim.Store.Repositories.Implementations;

internal sealed class ProductRepository: Repository<Product>, IProductRepository
{
    public ProductRepository(SuzimContextPostgres dbContext) : base(dbContext) { }
}