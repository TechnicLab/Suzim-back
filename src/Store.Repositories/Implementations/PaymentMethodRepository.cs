using Suzim.Store.Common.Entities;
using Suzim.Store.Postgres;
using Suzim.Store.Repositories.Abstract;
using Suzim.Store.Repositories.Abstract.Shared;

namespace Suzim.Store.Repositories.Implementations;

internal sealed class PaymentMethodRepository : Repository<PaymentMethod>, IPaymentMethodRepository
{
    public PaymentMethodRepository(SuzimContextPostgres dbContext) : base(dbContext) { }
}