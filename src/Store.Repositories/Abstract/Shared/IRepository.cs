using Suzim.Store.Common.Contracts;

namespace Suzim.Store.Repositories.Abstract.Shared;

public interface IRepository<TEntity> : IDbRepository
    where TEntity : class, IEntityWithId
{
    Task<TEntity?> GetById(Guid id, bool isOnlyActive = true, CancellationToken token = default);
    
    Task<(int total, IReadOnlyList<TEntity> items)> GetAll(int offset = 0, int limit = 100, bool isOnlyActive = true, CancellationToken token = default);
    
    void Add(TEntity entity);
    
    void Update(TEntity entity);
    
    void Delete(TEntity entity);
}