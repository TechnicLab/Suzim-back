using Microsoft.EntityFrameworkCore;
using Suzim.Store.Common.Contracts;
using Suzim.Store.Postgres;

namespace Suzim.Store.Repositories.Abstract.Shared;

public abstract class Repository<TEntity> : IRepository<TEntity>
    where TEntity : class, IEntityWithId, IAuditableEntity
{
    private readonly SuzimContextPostgres _dbContext;
    protected readonly DbSet<TEntity> DbSet;

    internal Repository(SuzimContextPostgres dbContext)
    {
        _dbContext = dbContext;
        DbSet = dbContext.Set<TEntity>();
    }
    
    public virtual async Task<TEntity?> GetById(Guid id, bool isOnlyActive = true, CancellationToken token = default)
    {
        var query = DbSet.AsQueryable();
        if (isOnlyActive)
        {
            query = query.NotDeletedAt();
        }

        return await query.FirstOrDefaultAsync(x => x.Id == id, token);
    }

    public virtual async Task<(int total, IReadOnlyList<TEntity> items)> GetAll(int offset = 0, int limit = 100, bool isOnlyActive = true, CancellationToken token = default)
    {
        var query = DbSet.AsQueryable();

        if (isOnlyActive)
        {
            query = query.Where(x => !x.DeletedAt.HasValue);
        }

        var count = await query.CountAsync(token);

        if (count > offset)
        {
            query = query
                .Skip(offset)
                .Take(limit);
        }

        var result = await query.ToListAsync(cancellationToken: token);

        return (count, result);
    }
    
    public virtual void Add(TEntity entity)
    {
        DbSet.Add(entity);
        _dbContext.SaveChanges();
    }

    public virtual void Update(TEntity entity)
    {
        _dbContext.Entry(entity).State = EntityState.Modified;
        _dbContext.SaveChanges();
    }

    public virtual void Delete(TEntity entity)
    {
        if (entity is not IAuditableEntity deletableAtEntity)
        {
            return;
        }

        deletableAtEntity.DeletedAt = DateTimeOffset.UtcNow;
        Update(entity);
    }
}