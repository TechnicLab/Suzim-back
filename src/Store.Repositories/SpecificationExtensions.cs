using Suzim.Store.Common.Contracts;

namespace Suzim.Store.Repositories;

public static class SpecificationExtensions
{
    public static IQueryable<TEntity> NotDeletedAt<TEntity>(this IQueryable<TEntity> query)
        where TEntity : class, IAuditableEntity => 
        query.Where(x => x.DeletedAt == new DateTimeOffset?());
}