namespace Suzim.Web.Client;

internal sealed class SuzimClient: ISuzimClient
{
    public SuzimClient(
        ICustomerClient customerClient,
        IPaymentMethodClient paymentMethodClient,
        IOrderClient orderClient,
        IProductClient productClient)
    {
        CustomerClient = customerClient;
        PaymentMethodClient = paymentMethodClient;
        OrderClient = orderClient;
        ProductClient = productClient;
    }

    public ICustomerClient CustomerClient { get; }
    
    public IPaymentMethodClient PaymentMethodClient { get; }
    
    public IOrderClient OrderClient { get; }
    
    public IProductClient ProductClient { get; }
}