using Microsoft.Extensions.DependencyInjection;

namespace Suzim.Web.Client.DI;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSuzimApiClients(this IServiceCollection services, Func<HttpClient> client)
    {
       
        services.AddHttpClient<IPaymentMethodClient, PaymentMethodClient>(nameof(PaymentMethodClient), _ =>new PaymentMethodClient(client.Invoke()));
        services.AddHttpClient<ICustomerClient, CustomerClient>(nameof(CustomerClient), _ =>new CustomerClient(client.Invoke()));
        services.AddHttpClient<IOrderClient, OrderClient>(nameof(OrderClient), _ => new OrderClient(client.Invoke()));
        services.AddHttpClient<IProductClient, ProductClient>(nameof(ProductClient), _ => new ProductClient(client.Invoke()));

        services.AddSingleton<ISuzimClient,SuzimClient>();

        return services;
    }
}