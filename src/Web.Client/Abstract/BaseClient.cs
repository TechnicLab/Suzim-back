namespace Suzim.Web.Client.Abstract;

public abstract class BaseClient
{
    public BaseClient()
    {
        
    }

    protected Task PrepareRequestAsync(
        HttpClient client,
        HttpRequestMessage request,
        string url,
        CancellationToken token)=> Task.CompletedTask;

    protected Task PrepareRequestAsync(
        HttpClient client,
        HttpRequestMessage request,
        System.Text.StringBuilder urlBuilder,
        CancellationToken token) => Task.CompletedTask;

    protected Task ProcessResponseAsync(
        System.Net.Http.HttpClient client,
        System.Net.Http.HttpResponseMessage response,
        CancellationToken token) => Task.CompletedTask;
}