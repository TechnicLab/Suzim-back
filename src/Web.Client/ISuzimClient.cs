namespace Suzim.Web.Client;

public interface ISuzimClient
{
    ICustomerClient CustomerClient { get; }
    
    IPaymentMethodClient PaymentMethodClient { get; }
    
    IOrderClient OrderClient { get; }
    
    IProductClient ProductClient { get; }
}