using Microsoft.EntityFrameworkCore;
using Suzim.Store.Common.Entities;

namespace Suzim.Store.Common;

public interface ISuzimContext : IDisposable
{
    /// <summary>
    /// Заказы
    /// </summary>
    DbSet<Order> Orders { get; }
    
    /// <summary>
    /// Заказчики
    /// </summary>
    DbSet<Customer> Customers { get; }
    
    /// <summary>
    /// Способы оплаты
    /// </summary>
    DbSet<PaymentMethod> PaymentMethods { get; }

    /// <summary>
    /// Продукты
    /// </summary>
    DbSet<Product> Products { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}