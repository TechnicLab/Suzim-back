using Suzim.Store.Common.Contracts;
using Suzim.Store.Common.Enums;

namespace Suzim.Store.Common.Entities;

public sealed class Order: IAuditableEntity, IEntityWithId
{
    public Order(
        Guid id,
        string number, 
        string address,
        OrderStatus status,
        Guid customerId,
        Guid paymentMethodId)
    {
        Id = id;
        Number = number;
        Address = address;
        Status = status;
        CustomerId = customerId;
        PaymentMethodId = paymentMethodId;
        CreatedAt = DateTimeOffset.UtcNow;
        ModifiedAt = DateTimeOffset.UtcNow;
    }
    
    public Guid Id { get; set; }
    
    /// <summary>
    /// Номер заказа
    /// </summary>
    public string Number { get; set; }
    
    
    /// <summary>
    /// Заказчик
    /// </summary>
    public Guid CustomerId { get; set; }
    public Customer? Customer { get; set; }

  
    /// <summary>
    /// Способ оплаты
    /// </summary>
    public Guid PaymentMethodId { get; set; }
    public PaymentMethod? PaymentMethod { get; set; }

    public List<Product> Products { get; set; } = new();

    /// <summary>
    /// Статус заказа
    /// </summary>
    public OrderStatus Status { get; set; }
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string Address { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}