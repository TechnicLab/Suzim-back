using Suzim.Store.Common.Contracts;

namespace Suzim.Store.Common.Entities;

public sealed class PaymentMethod : IEntityWithId, INamedEntity, IAuditableEntity
{
    public PaymentMethod(Guid id, string name)
    {
        Id = id;
        Name = name;
        CreatedAt = DateTimeOffset.UtcNow;
        ModifiedAt = DateTimeOffset.UtcNow;
    }

    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}