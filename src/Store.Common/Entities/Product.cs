using Suzim.Store.Common.Contracts;

namespace Suzim.Store.Common.Entities;

public sealed class Product : IEntityWithId, INamedEntity, IAuditableEntity
{
    public Product(
        Guid id,
        string name,
        decimal price,
        string? description)
    {
        Id = id;
        Name = name;
        Price = price;
        Description = description;
        CreatedAt = DateTimeOffset.UtcNow;
        ModifiedAt = DateTimeOffset.UtcNow;
    }

    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public decimal Price { get; set; }
    
    public string? Description { get; set; }

    public List<Order> Orders { get; set; } = new();
    
    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}