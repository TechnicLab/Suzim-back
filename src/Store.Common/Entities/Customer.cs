using Suzim.Store.Common.Contracts;

namespace Suzim.Store.Common.Entities;

/// <summary>
/// Заказчик
/// </summary>
public sealed class Customer : IEntityWithId, IHuman, IAuditableEntity
{
    public Customer(
        Guid id,
        string surname, 
        string name,
        string? patronymic,
        string? email,
        string phoneNumber)
    {
        Id = id;
        Surname = surname;
        Name = name;
        Patronymic = patronymic;
        Email = email;
        PhoneNumber = phoneNumber;
        CreatedAt = DateTimeOffset.UtcNow;
        ModifiedAt = DateTimeOffset.UtcNow;
    }

    public Guid Id { get; set; }
    
    public string Surname { get; set; }
    
    public string Name { get; set; }
    
    public string? Patronymic { get; set; }
    
    public string PhoneNumber { get; set; }
    
    public string? Email { get; set; }

    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}