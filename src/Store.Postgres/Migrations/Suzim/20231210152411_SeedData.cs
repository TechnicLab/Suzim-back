﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Suzim.Store.Postgres.Migrations.Suzim
{
    /// <inheritdoc />
    public partial class SeedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Surname = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Patronymic = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Price = table.Column<decimal>(type: "numeric", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Number = table.Column<string>(type: "text", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uuid", nullable: false),
                    PaymentMethodId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_PaymentMethods_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalTable: "PaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderProduct",
                columns: table => new
                {
                    OrdersId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProduct", x => new { x.OrdersId, x.ProductsId });
                    table.ForeignKey(
                        name: "FK_OrderProduct_Orders_OrdersId",
                        column: x => x.OrdersId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderProduct_Products_ProductsId",
                        column: x => x.ProductsId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Email", "ModifiedAt", "Name", "Patronymic", "PhoneNumber", "Surname" },
                values: new object[,]
                {
                    { new Guid("0479cf26-0cd1-4806-badd-42d154c60d2f"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5970), new TimeSpan(0, 0, 0, 0, 0)), null, "hans.mueller@example.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5970), new TimeSpan(0, 0, 0, 0, 0)), "Hans", "", "+49 1234 567890", "Müller" },
                    { new Guid("657ed505-0374-4b5e-abe9-d0360d5ab5d6"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5962), new TimeSpan(0, 0, 0, 0, 0)), null, "alice.smith@example.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5963), new TimeSpan(0, 0, 0, 0, 0)), "Alice", "", "+44 20 1234 5678", "Smith" },
                    { new Guid("9752037f-ec9e-4617-adca-e3a26fb97882"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5959), new TimeSpan(0, 0, 0, 0, 0)), null, "john.doe@example.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5960), new TimeSpan(0, 0, 0, 0, 0)), "John", "", "+1 555 555 5555", "Doe" },
                    { new Guid("acbc6b97-0ad9-43d1-a3e5-cd56a0e5b6ad"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5966), new TimeSpan(0, 0, 0, 0, 0)), null, "seung.hwan.kim@example.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5967), new TimeSpan(0, 0, 0, 0, 0)), "Seung", "Hwan", "+82 2 3456 7890", "Kim" },
                    { new Guid("f108e05f-10a4-4ac5-b26d-7c568905b914"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5965), new TimeSpan(0, 0, 0, 0, 0)), null, "maria.garcia@example.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5965), new TimeSpan(0, 0, 0, 0, 0)), "Maria", "Lopez", "+34 123 456 789", "Garcia" },
                    { new Guid("f24f98b1-1548-4006-8a17-80337f5c235e"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5951), new TimeSpan(0, 0, 0, 0, 0)), null, "test@test.com", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5954), new TimeSpan(0, 0, 0, 0, 0)), "Michael", "Petrovitch", "+ 7 777 777 77 77", "Zubenko" }
                });

            migrationBuilder.InsertData(
                table: "PaymentMethods",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name" },
                values: new object[,]
                {
                    { new Guid("5ac207b5-7a03-4e54-af61-dee1c88ad62e"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6037), new TimeSpan(0, 0, 0, 0, 0)), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6038), new TimeSpan(0, 0, 0, 0, 0)), "Mirpay" },
                    { new Guid("8b3e1beb-85f6-4895-a9cb-9e0bfe028354"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6042), new TimeSpan(0, 0, 0, 0, 0)), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6042), new TimeSpan(0, 0, 0, 0, 0)), "Kiwi" },
                    { new Guid("ed6fd19a-5bdc-4f90-9868-95e048a7c0ee"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6040), new TimeSpan(0, 0, 0, 0, 0)), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6040), new TimeSpan(0, 0, 0, 0, 0)), "Mastercard" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Description", "ModifiedAt", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("13274c18-b619-472d-9b85-949cdd266c06"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6006), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6006), new TimeSpan(0, 0, 0, 0, 0)), "Sony PlayStation 5", 50000m },
                    { new Guid("43597017-9aa4-4e16-b491-0eb4da5b07d9"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)), "Nespresso Coffee Machine", 5000m },
                    { new Guid("75bb054e-ee94-437a-8ba3-ee7d1124fd40"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6016), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6016), new TimeSpan(0, 0, 0, 0, 0)), "Amazon Echo", 1000m },
                    { new Guid("798315d6-e210-4938-a6e3-1e7fb9d706f4"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6010), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6011), new TimeSpan(0, 0, 0, 0, 0)), "LG OLED TV", 120000m },
                    { new Guid("84c22bce-f075-4f4a-bd38-074b999b8269"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6002), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6002), new TimeSpan(0, 0, 0, 0, 0)), "MacBook Pro", 200000m },
                    { new Guid("9774b54d-5dc4-4748-8033-db0de46aa406"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6008), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6009), new TimeSpan(0, 0, 0, 0, 0)), "Xbox Series X", 55000m },
                    { new Guid("9f420702-6a86-414b-bc7c-79c7c7d1ece7"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6004), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6004), new TimeSpan(0, 0, 0, 0, 0)), "Dell XPS", 150000m },
                    { new Guid("c8f3f6b6-1bc2-45bb-9085-dabb7b043b4d"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6000), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6000), new TimeSpan(0, 0, 0, 0, 0)), "Samsung Galaxy", 80000m },
                    { new Guid("f2f50f28-7bbe-4798-977c-ed5396a1dd22"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6012), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6012), new TimeSpan(0, 0, 0, 0, 0)), "Bose Noise Cancelling Headphones", 3000m },
                    { new Guid("fb22a290-4436-456d-8150-148964f93ad5"), new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5994), new TimeSpan(0, 0, 0, 0, 0)), null, "RandomDescription", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(5994), new TimeSpan(0, 0, 0, 0, 0)), "IPhone", 100000m }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "Address", "CreatedAt", "CustomerId", "DeletedAt", "ModifiedAt", "Number", "PaymentMethodId", "Status" },
                values: new object[,]
                {
                    { new Guid("3399ac6c-5b82-4a74-b741-29ac8788df69"), "пер.Вяземский", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6087), new TimeSpan(0, 0, 0, 0, 0)), new Guid("f24f98b1-1548-4006-8a17-80337f5c235e"), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6087), new TimeSpan(0, 0, 0, 0, 0)), "#2", new Guid("8b3e1beb-85f6-4895-a9cb-9e0bfe028354"), 301 },
                    { new Guid("c5c446af-9f82-40af-9ec6-4c12f619b46c"), "пер.Вяземский", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6102), new TimeSpan(0, 0, 0, 0, 0)), new Guid("657ed505-0374-4b5e-abe9-d0360d5ab5d6"), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6102), new TimeSpan(0, 0, 0, 0, 0)), "#3", new Guid("ed6fd19a-5bdc-4f90-9868-95e048a7c0ee"), 400 },
                    { new Guid("cf9efc85-dcf4-4f20-8cf9-a5989d2bca11"), "Ул.Пушкина", new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6067), new TimeSpan(0, 0, 0, 0, 0)), new Guid("f24f98b1-1548-4006-8a17-80337f5c235e"), null, new DateTimeOffset(new DateTime(2023, 12, 10, 15, 24, 11, 235, DateTimeKind.Unspecified).AddTicks(6068), new TimeSpan(0, 0, 0, 0, 0)), "#1", new Guid("5ac207b5-7a03-4e54-af61-dee1c88ad62e"), 101 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderProduct_ProductsId",
                table: "OrderProduct",
                column: "ProductsId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PaymentMethodId",
                table: "Orders",
                column: "PaymentMethodId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderProduct");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "PaymentMethods");
        }
    }
}
