using Microsoft.EntityFrameworkCore;
using Suzim.Store.Common;
using Suzim.Store.Common.Entities;
using Suzim.Store.Common.Enums;

namespace Suzim.Store.Postgres;

public sealed class SuzimContextPostgres : DbContext, ISuzimContext
{
    public SuzimContextPostgres(DbContextOptions<SuzimContextPostgres> options) : base(options) { }
    
    public DbSet<Order> Orders  => Set<Order>();
    
    public DbSet<Customer> Customers => Set<Customer>();
    
    public DbSet<PaymentMethod> PaymentMethods => Set<PaymentMethod>();
    
    public DbSet<Product> Products => Set<Product>();
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        base.OnModelCreating(modelBuilder);

       modelBuilder.Entity<Customer>().HasData(CustomerRecords);
       modelBuilder.Entity<Product>().HasData(ProductRecords);
       modelBuilder.Entity<PaymentMethod>().HasData(PaymentMethodRecords);
       modelBuilder.Entity<Order>().HasData(OrderRecords);
    }

    private List<Order> OrderRecords => new()
    {
        new Order(Guid.Parse("CF9EFC85-DCF4-4F20-8CF9-A5989D2BCA11"),
            "#1",
            "Ул.Пушкина",
            OrderStatus.Created,
            customerId: CustomerRecords[0].Id,
            paymentMethodId: PaymentMethodRecords[0].Id),
        new Order(Guid.Parse("3399AC6C-5B82-4A74-B741-29AC8788DF69"),
            "#2",
            "пер.Вяземский",
            OrderStatus.Received,
            customerId: CustomerRecords[0].Id,
            paymentMethodId: PaymentMethodRecords[2].Id),
        new Order(Guid.Parse("C5C446AF-9F82-40AF-9EC6-4C12F619B46C"),
            "#3",
            "пер.Вяземский",
            OrderStatus.Canceled,
            customerId: CustomerRecords[2].Id,
            paymentMethodId: PaymentMethodRecords[1].Id)
    };

    private List<PaymentMethod> PaymentMethodRecords => new()
    {
        new PaymentMethod(Guid.Parse("5AC207B5-7A03-4E54-AF61-DEE1C88AD62E"), "Mirpay"),
        new PaymentMethod(Guid.Parse("ED6FD19A-5BDC-4F90-9868-95E048A7C0EE"), "Mastercard"),
        new PaymentMethod(Guid.Parse("8B3E1BEB-85F6-4895-A9CB-9E0BFE028354"), "Kiwi"),
    };

    private List<Product> ProductRecords => new()
    {
        new Product(Guid.Parse("FB22A290-4436-456D-8150-148964F93AD5"), "IPhone", 100000M, "RandomDescription"),
        new Product(Guid.Parse("C8F3F6B6-1BC2-45BB-9085-DABB7B043B4D"), "Samsung Galaxy", 80000M, "RandomDescription"),
        new Product(Guid.Parse("84C22BCE-F075-4F4A-BD38-074B999B8269"), "MacBook Pro", 200000M, "RandomDescription"),
        new Product(Guid.Parse("9F420702-6A86-414B-BC7C-79C7C7D1ECE7"), "Dell XPS", 150000M, "RandomDescription"),
        new Product(Guid.Parse("13274C18-B619-472D-9B85-949CDD266C06"), "Sony PlayStation 5", 50000M, "RandomDescription"),
        new Product(Guid.Parse("9774B54D-5DC4-4748-8033-DB0DE46AA406"), "Xbox Series X", 55000M, "RandomDescription"),
        new Product(Guid.Parse("798315D6-E210-4938-A6E3-1E7FB9D706F4"), "LG OLED TV", 120000M, "RandomDescription"),
        new Product(Guid.Parse("F2F50F28-7BBE-4798-977C-ED5396A1DD22"), "Bose Noise Cancelling Headphones", 3000M, "RandomDescription"),
        new Product(Guid.Parse("43597017-9AA4-4E16-B491-0EB4DA5B07D9"), "Nespresso Coffee Machine", 5000M, "RandomDescription"),
        new Product(Guid.Parse("75BB054E-EE94-437A-8BA3-EE7D1124FD40"), "Amazon Echo", 1000M, "RandomDescription")
    };

    private List<Customer> CustomerRecords => new()
    {
        new Customer(Guid.Parse("F24F98B1-1548-4006-8A17-80337F5C235E"), "Zubenko", "Michael", "Petrovitch", "test@test.com", "+ 7 777 777 77 77"),
        new Customer(Guid.Parse("9752037F-EC9E-4617-ADCA-E3A26FB97882"), "Doe", "John", "", "john.doe@example.com", "+1 555 555 5555"),
        new Customer(Guid.Parse("657ED505-0374-4B5E-ABE9-D0360D5AB5D6"), "Smith", "Alice", "", "alice.smith@example.com", "+44 20 1234 5678"),
        new Customer(Guid.Parse("F108E05F-10A4-4AC5-B26D-7C568905B914"), "Garcia", "Maria", "Lopez", "maria.garcia@example.com", "+34 123 456 789"),
        new Customer(Guid.Parse("ACBC6B97-0AD9-43D1-A3E5-CD56A0E5B6AD"), "Kim", "Seung", "Hwan", "seung.hwan.kim@example.com", "+82 2 3456 7890"),
        new Customer(Guid.Parse("0479CF26-0CD1-4806-BADD-42D154C60D2F"), "Müller", "Hans", "", "hans.mueller@example.com", "+49 1234 567890")
    };
    
}