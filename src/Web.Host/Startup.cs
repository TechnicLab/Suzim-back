using System.Text.Json.Serialization;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Identity;
using Suzim.Business.DI;
using Suzim.Store.Postgres;
using Suzim.Store.Postgres.DI;
using Suzim.Web.Host.Infrastructure;

namespace Suzim.Web.Host;

internal sealed class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public static void ConfigureServices(IServiceCollection services)
    { 
    
        services.AddControllers()
            .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
        services.AddSuzimContextPostgres("SuzimConnection");

        services.AddDefaultIdentity<IdentityUser>()
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<IdentityContextPostgres>();
        
        services.AddAutoMapper(WebAssemblyReference.Assembly);
        services.AddBusinessModule();
        
        services.AddFluentValidationAutoValidation()
             .AddFluentValidationClientsideAdapters(); 
        services.AddValidatorsFromAssembly(WebAssemblyReference.Assembly);
        services.AddSwaggerServices();
    }

    public static void Configure(IApplicationBuilder app)
    {
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            options.RoutePrefix = string.Empty;
        });
        
        
        app.UseRouting();
        app.UseCors(builder => builder.WithOrigins("http://localhost:8080")
            .AllowAnyHeader()
            .AllowCredentials()
            .AllowAnyMethod());
        
        app.UseAuthorization();
        app.UseAuthentication();

        app.UseEndpoints(options =>
            options.MapControllers());
    }
}
