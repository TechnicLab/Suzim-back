namespace Suzim.Web.Contracts.Models;

/// <summary>
/// Модель для получения коллекции элементов
/// </summary>
public class PageResponse<T>
{
    public PageResponse(IReadOnlyCollection<T> items, int total)
    {
        Items = items;
        Total = total;
    }
    
    /// <summary>
    /// Коллекция элементов типа <see cref="T"/>
    /// </summary>
    public IReadOnlyCollection<T> Items { get; }

    /// <summary>
    /// Количество элементов
    /// </summary>
    public int Total { get; }
}