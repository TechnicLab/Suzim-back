using Suzim.Store.Common.Enums;
using Suzim.Web.Contracts.Models.Customer;
using Suzim.Web.Contracts.Models.PaymentMethod;
using Suzim.Web.Contracts.Models.Product;

namespace Suzim.Web.Contracts.Models.Order;

public class OrderResponse
{
    public Guid? Id { get; set; }
    
    public string? Number { get; set; }
    
    public OrderStatus Status { get; set; }
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string? Address { get; set; }
    
    /// <summary>
    /// Заказчик
    /// </summary>
    public CustomerResponse? Customer { get; set; }
  
    /// <summary>
    /// Способ оплаты
    /// </summary>
    public PaymentMethodResponse? PaymentMethod { get; set; }

    public IReadOnlyList<ProductResponse> Products { get; set; } = Array.Empty<ProductResponse>();
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}