using System.Text.Json.Serialization;
using Suzim.Store.Common.Enums;

namespace Suzim.Web.Contracts.Models.Order;

public class OrderUpdateRequest
{
    /// <summary>
    /// Номер заказа
    /// </summary>
    public string? Number { get; set; }

    /// <summary>
    /// Способ оплаты
    /// </summary>
    public Guid? PaymentMethodId { get; set; }
    
    public OrderStatus Status { get; set; }
    
    /// <summary>
    /// Адрес
    /// </summary>
    public string? Address { get; set; }
}