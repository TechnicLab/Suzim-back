namespace Suzim.Web.Contracts.Models.Order;

public class OrderCreateRequest
{
    public string? Number { get; set; }
    
    public string? Address { get; set; }
    
    public Guid? PaymentMethodId { get; set; }
    
    public Guid? CustomerId { get; set; }
}