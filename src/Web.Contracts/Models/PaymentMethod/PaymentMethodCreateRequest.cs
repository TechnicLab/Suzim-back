namespace Suzim.Web.Contracts.Models.PaymentMethod;

public class PaymentMethodCreateRequest
{
    public string? Name { get; set; }
}