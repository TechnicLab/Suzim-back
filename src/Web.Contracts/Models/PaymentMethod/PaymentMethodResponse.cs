namespace Suzim.Web.Contracts.Models.PaymentMethod;

public class PaymentMethodResponse
{
    /// <summary>
    /// Ид метода оплаты
    /// </summary>
    public Guid? Id { get; set; }
    
    /// <summary>
    /// Метод оплаты
    /// </summary>
    public string? Name { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}