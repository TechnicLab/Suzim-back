namespace Suzim.Web.Contracts.Models.PaymentMethod;

public class PaymentMethodUpdateRequest
{ 
    /// <summary>
    /// Метод оплаты
    /// </summary>
    public string? Name { get; set; }
}