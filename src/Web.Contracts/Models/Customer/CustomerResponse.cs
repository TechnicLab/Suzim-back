namespace Suzim.Web.Contracts.Models.Customer;

public class CustomerResponse
{
    public Guid? Id { get; set; }
  
    public string? Surname { get; set; }
    
    public string? Name { get; set; }
    
    public string? Patronymic { get; set; }
    
    public string? PhoneNumber { get; set; }
    
    public string? Email { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }
    
    public DateTimeOffset? DeletedAt { get; set; }
}