namespace Suzim.Web.Contracts.Models.Customer;

public class CustomerUpdateRequest
{
    public string? Surname { get; set; }
    
    public string? Name { get; set; }
    
    public string? Patronymic { get; set; }
    
    public string? PhoneNumber { get; set; }
    
    public string? Email { get; set; }
}