namespace Suzim.Web.Contracts.Models.Product;

public class ProductCreateRequest
{
    public string? Name { get; set; }
    
    public decimal? Price { get; set; }
    
    public string? Description { get; set; }
}