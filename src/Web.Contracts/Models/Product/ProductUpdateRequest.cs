namespace Suzim.Web.Contracts.Models.Product;

public class ProductUpdateRequest
{
    public string? Name { get; set; }
    
    public decimal? Price { get; set; }
    
    public string? Description { get; set; }
}