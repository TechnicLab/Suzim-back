namespace Suzim.Web.Contracts.Models;

/// <summary>
/// Модель для постраничного вывода коллекции
/// </summary>
public class PageFilterRequest
{
    /// <summary>
    /// Количество элементов
    /// </summary>
    public int Limit { get; set; } = 25;

    /// <summary>
    /// Отступ
    /// </summary>
    public int Offset { get; set; } = 0;
}