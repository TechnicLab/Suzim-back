namespace Suzim.Web.Contracts.Models;

public class ExcelReportRequest
{
    public DateTimeOffset StartUtc { get; set; }
    
    public DateTimeOffset EncUtc { get; set; }
    
    public Guid? CustomerId { get; set; }
    
    public bool IsOnlyActive { get; set; }
}